" ----------------------------------------------------------------------------
"           FILE: init.vim
"    DESCRIPTION: neovim configuration file
"         AUTHOR: Leslie King <zoliky@gmail.com>
" ----------------------------------------------------------------------------

" Use the same configuration files and plugins as Vim
set runtimepath+=~/.vim,~/.vim/after
set packpath^=~/.vim
source ~/.vimrc
