# Dotfiles

## Installation

```bash
$ cd ~
$ git clone https://github.com/zoliky/dotfiles.git
$ cd dotfiles
$ make install
```

## Vim and Neovim

### Managing Plugins

```vimscript
:PackUpdate
:PackClean
```

### Enabling Python Support in Neovim

```bash
$ pip3 install --user --upgrade neovim
```
